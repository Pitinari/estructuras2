{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}

data TTree k v = Node k (Maybe v) (TTree k v) (TTree k v) (TTree k v)
               |Leaf k v
               |E
                deriving Show

search :: Ord k => [k] -> TTree k v -> Maybe v
search _ E = Nothing
search [] _ = Nothing
search [k] (Leaf k' v) | k == k' = Just v
                       | otherwise = Nothing
search (k:ks) (Leaf _ _) = Nothing  
search xs@(k:ks) (Node k' v l c r) | k == k' && ks == [] = v
                                   | k == k' = search ks c
                                   | k < k' = search xs l
                                   | otherwise = search xs r

insert :: Ord k => [k] -> v -> TTree k v -> TTree k v
insert [] _ t = t
insert [k] v E = Leaf k v
insert (k:ks) v E = Node k Nothing E (insert ks v E) E
insert xs@(k:ks) v (Leaf k' v') | k == k' && ks == [] = (Leaf k v)
                                | k == k' = Node k (Just v') E (insert ks v E) E
                                | k < k' = Node k' (Just v') (insert xs v E) E E
                                | otherwise = Node k' (Just v') E E (insert xs v E)
insert xs@(k:ks) v (Node k' v' l c r) | k == k' && ks == [] = Node k (Just v) l c r
                                      | k == k' = Node k' v' l (insert ks v c) r
                                      | k < k' = Node k' v' (insert xs v l) c r
                                      | otherwise = Node k' v' l c (insert xs v r)

delete :: Ord k => [k] -> TTree k v -> TTree k v
delete _ E = E
delete [] t = t
delete [k] l@(Leaf k' _) | k == k' = E
                         | otherwise = l
delete (k:ks) l@(Leaf _ _) = l
delete xs@(k:ks) (Node k' v l c r) | k == k' && ks == [] = Node k Nothing l c r
                                   | k == k' = fixTTree $ Node k' v l (delete ks c) r
                                   | k < k' = fixTTree $ Node k' v (delete xs l) c r
                                   | otherwise = fixTTree $ Node k' v l c (delete xs r)
                                   where fixTTree (Node _ Nothing E E E) = E
                                         fixTTree (Node k (Just v) E E E) = Leaf k v
                                         fixTTree t = t
keys :: Ord k => TTree k v -> [[k]]                                         
keys E = []
keys (Leaf k v) = [[k]]
keys (Node k v l c r) = case v of
                             Nothing -> keys l ++ ((k:) <$> keys c) ++ keys r
                             Just v  -> keys l ++ ([k] : ((k:) <$> keys c)) ++ keys r             
                             

class Dic k v d | d -> k v where
  vacio     :: d
  insertar  :: Ord k => k -> v -> d -> d
  buscar    :: Ord k => k -> d -> Maybe v
  eliminar  :: Ord k => k -> d -> d
  claves    :: Ord k => d -> [(k,v)]

instance Ord k => Dic [k] v (TTree k v) where
  vacio = E
  insertar = insert
  buscar = search
  eliminar = delete
  claves E = []
  claves (Leaf k v) = [([k],v)]
  claves (Node k v l c r) = case v of
                             Nothing -> claves l ++ ((goodCons k) <$> claves c) ++ claves r
                             Just v  -> claves l ++ (([k],v) : ((goodCons k) <$> claves c)) ++ claves r             
                             where goodCons k x = ( k : fst x, snd x)



t = Node 'r' Nothing 
  E 
  (Node 'e' (Just 16) 
    (Node 'a' Nothing 
      E
      (Leaf 's' 1)
      E
    )
    (Node 'o' (Just 2) 
      (Leaf 'd' 9)
      E
      (Leaf 's' 4)
    )
    E
  )
  (Node 's' Nothing 
    E
    (Node 'i' (Just 4) 
      (Leaf 'e' 8)
      (Leaf 'n' 7)
      E
    )
    E
  )
