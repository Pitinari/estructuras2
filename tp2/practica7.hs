import Par
import Seq

--1 a
promedios :: Seq Int -> Seq Float
promedios s = let (l, r) = scanS + 0 s
                  seq = appendS l (singleton r)
              in tabulateS (div i (reduceS + 0 (takeS seq i))) seq
-- hacer sin appendS

--1 b
mayor :: Int -> Int -> Int
mayor a b = if a > b then a else b

mayores :: Seq Int -> Int
mayores s = scan mayor (minBound :: Int) s


--2
matrixMult :: (Int, Int, Int, Int) -> (Int, Int, Int, Int) -> (Int, Int, Int, Int)
matrixMult (a,b,c,d) (w,x,y,z) = (a*w+b*y, a*x+b*z, c*w+d*y, c*x+d*z)

fibSeq :: Seq s => Int -> s Int
fibSeq n = mapS (\(a,b,c,d) -> a) (fst (scanS matrixMult (1, 1, 1, 0) (tabulateS (\_ -> (1,1,1,0)) n)))

--3
aguaHist :: Seq s => s Int -> Int

--4
data Paren = Open | Close

matchP :: Seq s => s Paren -> (Int, Int)
matchP s = (1,1)

matchParen s = matchP s == (0, 0)

--5
sccml :: Seq s => s Int → Int

--6
cantMultiplos :: Seq s => s Int -> Int

--7
merge :: Seq s => (a -> a -> Ordering) -> s a -> s a -> s a
sort :: Seq s => (a -> a -> Ordering) -> s a -> s a
maxE :: Seq s => (a -> a -> Ordering) -> s a -> a,
maxS :: Seq s => (a -> a -> Ordering) -> s a -> Int
group:: Seq s => (a -> a -> Ordering) -> s a -> s a
collect :: Seq s => s (a, b) -> s (a, s b)

--8
datosIngreso :: Seq s => s (String, s Int) -> s (Int, Int)

--9
countCaract:: Seq s => s (s Char) -> s (Char, Int)

huffman :: Seq s => s (s Char) -> s (Int, s Char)
