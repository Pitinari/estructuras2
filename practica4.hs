data Tree a = EM | N (Tree a) a (Tree a) deriving Show

completo :: a -> Int -> Tree a
completo x 0 = EM
completo x n = let child = completo x (n-1) 
	       in N child x child

balanceado :: a -> Int -> Tree a
